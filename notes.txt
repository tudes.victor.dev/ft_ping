http://abcdrfc.free.fr/rfc-vf/rfc0792.html

toute la doc qu'il faut pour comprendre.

format de message icmp :
Version :	4
IHL : 	Longueur d'en-tête Internet en mots de 32-bits.
Type de Service : 	0
Longueur Totale :	Longueur totale du datagramme en octets.
Identification :
Bits Contrôles :
Fragment Offset :	
Utilisés par le mécanisme de fragmentation, voir [1].
Durée de vie :	Durée de vie du datagramme en secondes; ce champ est diminué d'une unité par chaque module IP traversé dans lesquels le datagramme est traité, la valeur dans ce champ doit être au moins égale au nombre maximum de routeurs que ce datagramme est sensé traverser jusqu'à sa destination finale.
Protocole :	ICMP = 1
Checksum :	Le complément à un sur 16 bits de la somme des compléments à un de l'en-tête Internet pris par mots de 16 bits. Lors du calcul du Checksum, le champ destiné à recevoir ce Checksum sera laissé à zéro. Ce mécanisme de Checksum sera changé dans le futur. 
Adresse source :	L'adresse du routeur ou hôte qui compose le message ICMP. Sauf mention contraire, celle-ci peut être toute adresse de routeur.
Adresse destinataire :	L'adresse du routeur ou hôte à qui le message doit être envoyé.


Pour creer une socket :
socket(AF_INET, SOCK_RAW, IPPROTO_ICMP)
https://linux.die.net/man/2/setsockopt : pour set les options de sockets.
