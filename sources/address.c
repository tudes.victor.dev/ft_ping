/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   address.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: victortudes <victortudes@student.42.fr>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/01 19:28:59 by victortudes       #+#    #+#             */
/*   Updated: 2021/10/18 12:00:58 by victortudes      ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ping.h"

/*
** Parser et check des adresses
*/

/*
** parse les arguments, s'arrete au premier truc qui a pas l'air d'etre une option
*/

void	get_address(char **dest, char **argv)
{
	int i = 1;

	while (argv[i])
	{
		if (argv[i][0] != '-' && check_previous_argument(argv, i) == SUCCESS_CODE)
		{
			*dest = argv[i];
			return;
		}
		i++;
	}
	error_output_and_exit(USAGE);
}

/*
** check l'arg precedent pour s'assurer que c'est pas une option qui demande une valeur en plus
** genre -t ou -c
*/

char	check_previous_argument(char **argv, int i)
{
	if (i == 1)
		return (SUCCESS_CODE);
	i -= 1;
	if (argv[i][0] != '-')
		return (SUCCESS_CODE);
	if (argv[i][1] != 'c' && argv[i][1] != 't' && argv[i][1] != 'i')
		return (SUCCESS_CODE);
	return (ERROR_CODE);
}

/*
** check l'ip rentree par le user avec getaddrinfo, si ca existe, save l'adresse
*/

void	interpret_address(char *input)
{
	char				address[INET_ADDRSTRLEN];
	struct addrinfo		*res = NULL;
	struct addrinfo		hints;
	

	ft_bzero(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	if (getaddrinfo(input, NULL, &hints, &res) != 0)
		fprintf(stderr, UNKNOWN_ADDR_ERROR, input);
	else
	{
		ft_memcpy(&g_env.sockaddr, res->ai_addr, sizeof(struct sockaddr_in));
		inet_ntop(AF_INET, &(g_env.sockaddr.sin_addr), address, INET_ADDRSTRLEN);
		g_env.address = ft_strdup(address);
		reverse_dns();
	}
	if (res)
		freeaddrinfo(res);
}

/*
** reverse DNS pour chopper l'adresse ip derriere le nom de domainem si y'en a pas, ca copie l'adresse originale
*/

void	reverse_dns(void)
{
	char		reversed_address[NI_MAXHOST];

	if (getnameinfo((struct sockaddr *)&g_env.sockaddr, sizeof(struct sockaddr_in),
		reversed_address, sizeof(reversed_address), NULL, 0, NI_NAMEREQD) < 0)
	{
		g_env.reversed_address = ft_strdup(g_env.address);
	}
	else
		g_env.reversed_address = ft_strdup(reversed_address);
}