/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: victortudes <victortudes@student.42.fr>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/01 19:29:01 by victortudes       #+#    #+#             */
/*   Updated: 2021/10/18 12:01:00 by victortudes      ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ping.h"

/*
** gere le display et les outputs
*/

/*
** display le header de la fonction ping avec les infos de l'adresse videes
*/

void	display_header(void)
{
	printf("PING %s (%s) 56(84) bytes of data.\n", g_env.user_requested_address, g_env.address);
}

/*
display les stats finales, genre les packets envoyes, recus et perdus, et les infos 
** temportelles genre round-trip
*/

void	display_ending_stats(void)
{
	int		lost_packets = 0;
	double	total_time;

	if (g_env.sent_packets != 0)
	{
		lost_packets = 100 - ((g_env.received_packets * 100) / g_env.sent_packets);
		save_current_time(&g_env.ending_time);
		printf("\n--- %s ping statistics ---\n", g_env.user_requested_address);
		total_time = compute_elapsed_time(g_env.starting_time, g_env.ending_time);
		if (!g_env.error_packets)
		{
			printf("%d packets transmitted, %d received, %d%% packet loss, time %.0lfms\n",
				g_env.sent_packets, g_env.received_packets, lost_packets, total_time);
		}
		else
		{
			printf("%d packets transmitted, %d received, +%d errors, %d%% packet loss, time %.0lfms\n",
				g_env.sent_packets, g_env.received_packets, g_env.error_packets, lost_packets, total_time);
		}
		if (g_env.received_packets != 0)
		{
			printf("rtt min/avg/max/mdev = %.3lf/%.3lf/%.3lf/%.3lf ms\n", g_env.min_time, g_env.sum_time / g_env.sent_packets,
				g_env.max_time, time_standard_deviation());
		}
	}
}

/*
** appelle la loop pour display la sequence d'info actuelle apres un pring reussi
** prints la sequence number, la latence et la ttl
*/

void	display_sequence(int received_bytes, t_reply reply, struct timeval start_timestamp, struct timeval end_timestamp)
{
	short		reply_ttl;
	double		time_elapsed;
	struct ip	*packet_content;

	packet_content = (struct ip *)reply.receive_buffer;
	reply_ttl = (short)packet_content->ip_ttl;
	time_elapsed = compute_elapsed_time(start_timestamp, end_timestamp);
	if (!(g_env.flags & F_FLAG))
	{
		if (ft_strcmp(g_env.address, g_env.user_requested_address))
		{
			printf("%lu bytes from %s (%s): icmp_seq=%d ttl=%d time=%.2lf ms\n", received_bytes - sizeof(struct ip),
				g_env.reversed_address, g_env.address, g_env.seq, reply_ttl, time_elapsed);
		}
		else
		{
			printf("%lu bytes from %s: icmp_seq=%d ttl=%d time=%.2lf ms\n", received_bytes - sizeof(struct ip),
				g_env.address, g_env.seq, reply_ttl, time_elapsed);
		}
	}
	else
	{
		ft_putchar('\b');
		fflush(stdout);
	}
	save_elapsed_time(time_elapsed);
}

/*
** display les infos sur un ping failed a cause d'une ttl depassee
*/

void	display_exceeded_sequence(t_reply reply)
{
	struct ip			*packet_content;
	char				ip[INET_ADDRSTRLEN];
	char				hostname[NI_MAXHOST];
	struct sockaddr_in	tmp_sockaddr;

	packet_content = (struct ip *)reply.receive_buffer;
	inet_ntop(AF_INET, &(packet_content->ip_src), ip, INET_ADDRSTRLEN);
	tmp_sockaddr.sin_addr = packet_content->ip_src;
	tmp_sockaddr.sin_family = AF_INET;
	tmp_sockaddr.sin_port = 0;
	if (!(g_env.flags & F_FLAG))
	{
		if (g_env.flags & V_FLAG)
			printf("ft_ping: TTL set to %d seems insufficient.\n", g_env.ttl);
		if (getnameinfo((struct sockaddr *)&tmp_sockaddr, sizeof(struct sockaddr_in),
			hostname, sizeof(hostname), NULL, 0, NI_NAMEREQD) >= 0)
		{
			printf("From %s (%s): icmp_seq=%d Time to live exceeded\n", hostname, ip, g_env.seq);
		}
		else
		{
			printf("From %s: icmp_seq=%d Time to live exceeded\n", ip, g_env.seq);
		}
	}
	else
	{
		ft_putchar('\b');
		ft_putchar('E');
		fflush(stdout);
	}
}

/*
** qui s'il y a -v, print tous les flags en debut d'execution d'une maniere
** comprehensibles
*/

void	display_flags(void)
{
	printf("ft_ping: Verbose");
	if (g_env.flags & T_FLAG)
		printf(", TTL (Time to live) set to %d", g_env.ttl);
	if (g_env.flags & C_FLAG)
		printf(", number of packets, %d", g_env.count);
	if (g_env.flags & I_FLAG)
		printf(", interval between packets, %.3lfs", g_env.interval);
	if (g_env.flags & F_FLAG)
		printf(", flood mode");
	printf("\n");
}

/*
** print le message d'erreur sur la sortie standard
*/

void	error_output(char *message)
{
	fprintf(stderr, "%s\n", message);
}

/*
** print le message donne sur la sortie d'erreur standard, free toute la
** memoire et exit avec un code 1
*/

void	error_output_and_exit(char *message)
{
	fprintf(stderr, "%s\n", message);
	free_memory();
	exit(1);
}