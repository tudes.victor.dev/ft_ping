/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flags.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: victortudes <victortudes@student.42.fr>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/01 19:29:05 by victortudes       #+#    #+#             */
/*   Updated: 2021/10/18 12:00:59 by victortudes      ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ping.h"

/*
** gere les flags
*/

/*
** parse les arguments, stop a chaque fois que le premier char d'un arg est '-'.
** gere le flag avec store_flags
*/

int		parse_flags(char **argv)
{
	int i = 1;
	int flags = 0;

	while (argv[i])
	{
		if (argv[i][0] == '-')
			store_flags(&flags, argv, i);
		i++;
	}
	return (flags);
}

/*
** parse l'arg, traite chaque char comme une option. Si ca reconnait l'option
** active le flag correspondant. Sinon, le programme s'arrete
*/

void	store_flags(int *flags, char **argv, int index)
{
	int i = 1;

	while (argv[index][i])
	{
		switch (argv[index][i])
		{
			case 'v':
				*flags |= V_FLAG;
				break;
			case 'h':
				*flags |= H_FLAG;
				break;
			case 'c':
				*flags |= C_FLAG;
				get_count(argv, index);
				break;
			case 't':
				*flags |= T_FLAG;
				get_time_to_live(argv, index);
				break;
			case 'i':
				*flags |= I_FLAG;
				get_interval(argv, index);
				break;
			case 'f':
				*flags |= F_FLAG;
				handle_flood(*flags);
				break;
			default:
				*flags |= BAD_FLAG;
				fprintf(stderr, BAD_FLAG_ERROR, argv[index][i]);
				return;
		}
		i++;
	}
}

/*
** parse l'arg apres -t pour chopper la valeur de TTL, et check les erreurs
*/

void	get_time_to_live(char **argv, int index)
{
	int		ttl;

	if ((argv[index][1] == 't' && argv[index][2] == 0)
		&& (argv[index + 1] != NULL))
	{
		ttl = atoi(argv[index + 1]);
		if (ttl <= 255 && ttl > 0)
		{
			g_env.ttl = ttl;
			return;
		}
	}
	error_output_and_exit(BAD_TTL_ERROR);
}

/*
** parse l'arg apres -c pour chopper le nombre de paquets que le user choisit,
** cet check les erreurs
*/

void	get_count(char **argv, int index)
{
	long	count;

	if ((argv[index][1] == 'c' && argv[index][2] == 0)
		&& (argv[index + 1] != NULL))
	{
		count = atol(argv[index + 1]);
		if (count > 0 && count <= INT32_MAX)
		{
			g_env.count = (int)count;
			return;
		}
	}
	error_output_and_exit(BAD_COUNT_ERROR);
}

/*
** parse apres -i pour choper l'interval voulu et check les erreurs
*/

void	get_interval(char **argv, int index)
{
	double	interval;

	if ((argv[index][1] == 'i' && argv[index][2] == 0)
		&& (argv[index + 1] != NULL))
	{
		interval = atof(argv[index + 1]);
		if (interval >= 0 && interval <= 2147483)
		{
			g_env.interval = interval;
			return;
		}
	}
	error_output_and_exit(BAD_INTERVAL_ERROR);
}

/*
** -f option, s'assure que l'interval est a 0 et qu'il y a pas eu 
** de -i avant
*/

void	handle_flood(int flags)
{
	if (!(flags & I_FLAG))
		g_env.interval = 0;
}