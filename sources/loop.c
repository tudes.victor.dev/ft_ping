/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   loop.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: victortudes <victortudes@student.42.fr>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/01 19:29:09 by victortudes       #+#    #+#             */
/*   Updated: 2021/10/18 12:01:00 by victortudes      ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ping.h"

/*
** ping loop
*/

/*
** init les pre;ieres parts du paquest ICMP echp request a envoyer a une adresse
** dest en faisant des swap de bytes pour avoir le bon endian
*/

void			initialise_packet(struct s_packet *packet, struct timeval current_time)
{
	ft_bzero(packet, sizeof(t_packet));
	packet->icmp_header.icmp_type = ICMP_ECHO;
	packet->icmp_header.icmp_code = 0;
	packet->icmp_header.icmp_seq = BSWAP16(g_env.seq);
	packet->icmp_header.icmp_id = BSWAP16(g_env.process_id);
	ft_memcpy(&packet->icmp_header.icmp_dun, &(current_time.tv_sec), sizeof(current_time.tv_sec));
	packet->icmp_header.icmp_cksum = checksum(packet, sizeof(*packet));
}

/*
** somme de controle d'un unsigned short et return le resultat. fait pendant la creation du paquet
*/

unsigned short	checksum(void *address, int len)
{
	unsigned short	*buff;
	unsigned long	sum;

	buff = (unsigned short *)address;
	sum = 0;
	while (len > 1)
	{
		sum += *buff;
		buff++;
		len -= sizeof(unsigned short);
	}
	if (len)
		sum += *(unsigned char *)buff;
	sum = (sum >> 16) + (sum & 0xFFFF);
	sum += (sum >> 16);
	return ((unsigned short)~sum);
}

/*
** initles differentes variables de t_reply qui sera utilisee pour stocker la
** reponse a notre paquet de l'adresse ip dest
*/

void			initialise_reply(t_reply *reply)
{
	ft_bzero(reply, sizeof(t_reply));
	reply->iov.iov_base = reply->receive_buffer;
	reply->iov.iov_len = sizeof(reply->receive_buffer);
	reply->msghdr.msg_name = g_env.address;
	reply->msghdr.msg_iov = &reply->iov;
	reply->msghdr.msg_iovlen = 1;
	reply->msghdr.msg_control = &reply->control;
	reply->msghdr.msg_controllen = sizeof(reply->control);
}

/*
** envoie le paquet a l'adresse det et gere les erreurs de sendto
*/

char			send_packet(t_packet *packet)
{
	ssize_t sent_bytes;

	sent_bytes = sendto(g_env.socket_fd, packet, sizeof(*packet), 0,
		(struct sockaddr*)&g_env.sockaddr, sizeof(g_env.sockaddr));
	if (sent_bytes <= 0)
	{
		if (errno == ENETUNREACH)
			error_output(NO_CONNEXION_ERROR);
		else
			error_output(SENDTO_ERROR);
		return (ERROR_CODE);
	}
	if (g_env.flags & F_FLAG)
	{
		ft_putchar('.');
		fflush(stdout);
	}
	return (SUCCESS_CODE);
}

/*
** store la reponse dans la struct reply, et gere les erreurs de recvmsg 
*/

char			receive_reply(t_reply *reply)
{
	reply->received_bytes = recvmsg(g_env.socket_fd, &(reply->msghdr), 0);
	if (reply->received_bytes > 0)
	{
		return (check_reply(reply));
	}
	else
	{
		if ((errno == EAGAIN || errno == EWOULDBLOCK || errno == EINTR))
		{
			if (g_env.flags & V_FLAG)
				error_output(TIMEOUT_ERROR);
		}
		else
			error_output(RECV_ERROR);
		return (ERROR_CODE);
	}
	return (SUCCESS_CODE);
}

/*
** check le paquet de reponse pour etre sur que c'est bien ce qu'on veut
*/

char			check_reply(t_reply *reply)
{
	struct ip	*packet_content;

	packet_content = (struct ip *)reply->receive_buffer;
	if (packet_content->ip_p != IPPROTO_ICMP)
	{
		if (g_env.flags & V_FLAG)
			error_output(REPLY_ERROR);
		return (ERROR_CODE);
	}
	reply->icmp = (struct icmp *)(reply->receive_buffer + (packet_content->ip_hl << 2));
	if (reply->icmp->icmp_type == 11 && reply->icmp->icmp_code == 0)
	{
		return (TTL_EXCEEDED_CODE);
	}
	else if (BSWAP16(reply->icmp->icmp_id) != g_env.process_id || BSWAP16(reply->icmp->icmp_seq) != g_env.seq)
	{
		initialise_reply(reply);
		return (receive_reply(reply));
	}
	return (SUCCESS_CODE);
}

/*
** loop pour envoyer des paquets a la dest et attendre une reponse jusqu'a une erreur
** ou un ctrl C
*/

void			ping_loop(void)
{
	t_packet			packet;
	struct timeval		current_start_timestamp;
	struct timeval		current_ending_timestamp;
	t_reply				reply;
	char				check;

	save_current_time(&g_env.starting_time);
	while(1)
	{
		save_current_time(&current_start_timestamp);
		initialise_packet(&packet, current_start_timestamp);
		g_env.sent_packets++;
		check = send_packet(&packet);
		if (check == SUCCESS_CODE)
		{
			initialise_reply(&reply);
			check = receive_reply(&reply);
			if (check == SUCCESS_CODE)
			{
				g_env.received_packets++;
				save_current_time(&current_ending_timestamp);
				display_sequence(reply.received_bytes, reply, current_start_timestamp, current_ending_timestamp);;
			}
			else if (check == TTL_EXCEEDED_CODE)
			{
				display_exceeded_sequence(reply);
				g_env.error_packets++;
			}
		}
		g_env.seq++;
		if (g_env.count)
		{
			g_env.count--;
			if (g_env.count == 0)
				break;
		}
		wait_interval(current_start_timestamp);
	}
}