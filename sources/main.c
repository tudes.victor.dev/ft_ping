/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: victortudes <victortudes@student.42.fr>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/01 19:29:13 by victortudes       #+#    #+#             */
/*   Updated: 2021/10/18 12:01:01 by victortudes      ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ping.h"

/*
** setup toute la struc ping, parse les args et les stocke ou il faut
*/

t_env g_env = {0};

void	init_parameters(char **argv)
{
	g_env.ttl = 64;
	g_env.count = 0;
	g_env.interval = 1;
	g_env.flags = parse_flags(argv);
	if (g_env.flags & H_FLAG || g_env.flags & BAD_FLAG)
	{
		error_output(USAGE);
		exit(1);
	}
	else
	{
		g_env.process_id = getpid();
		g_env.seq = 1;
		g_env.sent_packets = 0;
		g_env.received_packets = 0;
		g_env.error_packets = 0;
		g_env.socket_fd = -1;
		g_env.sum_time = 0;
		g_env.max_time = 0;
		g_env.min_time = 0;
		g_env.square_sum_time = 0;
		get_address(&(g_env.user_requested_address), argv);
		interpret_address(g_env.user_requested_address);
	}
}

/*
** free la memoire allouee
*/

void	free_memory(void)
{
	if (g_env.address)
		free(g_env.address);
	if (g_env.reversed_address)
		free(g_env.reversed_address);
}

/*
** bah, main
*/

int		main(int argc, char **argv)
{
	if (argc == 1)
		error_output(USAGE);
	else
	{
		init_parameters(argv);
		if (g_env.flags & V_FLAG)
			display_flags();
		if (g_env.address)
			create_socket();
		if (g_env.socket_fd != -1)
		{
			set_signals();
			display_header();
			ping_loop();
			display_ending_stats();
		}
		free_memory();
	}
	return (0);
}