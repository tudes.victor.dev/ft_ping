/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   signal.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: victortudes <victortudes@student.42.fr>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/01 19:29:19 by victortudes       #+#    #+#             */
/*   Updated: 2021/10/18 12:01:02 by victortudes      ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ping.h"

/*
**  gere les signaux.
*/

/*
** definit les signaux requis pour le programme
*/

void	set_signals(void)
{
	signal(SIGINT, &handle_interrupt_signal);
	signal(SIGALRM, &handle_alarm_signal);
}

/*
** gere le ctrl C avec SIGINT, free et quitte avec exit code 0 
** parce que tout a marche
*/

void	handle_interrupt_signal(int signal)
{
	(void)signal;
	display_ending_stats();
	free_memory();
	exit(0);
}

/*
** gere le ctrl C avec SIGINT, free et quitte avec exit code 0 
** parce que tout a marche
*/

void	handle_alarm_signal(int signal)
{
	(void)signal;
	fprintf(stderr, "Alarm used\n");
}