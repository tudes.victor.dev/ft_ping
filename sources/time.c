/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   time.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: victortudes <victortudes@student.42.fr>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/01 19:29:23 by victortudes       #+#    #+#             */
/*   Updated: 2021/10/18 12:01:03 by victortudes      ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ping.h"

/*
** gere le temps
*/

/*
** utilise dans la loop, pour s'assurer qu'il y ait [g_env.interval] secondes
** entre chaque paquet envoye, defaut = 1s
*/

void			wait_interval(struct timeval start)
{
	struct timeval	current_time;
	struct timeval	goal_time;

	if (g_env.interval)
	{
		current_time = start;
		goal_time.tv_sec = current_time.tv_sec + (long)g_env.interval;
		goal_time.tv_usec = current_time.tv_usec + (long)((g_env.interval - (long)g_env.interval) * 1000000);
		while (timercmp(&current_time, &goal_time, <))
		{
			save_current_time(&current_time);
		}
	}
}

/*
** temps entre ping et pong
*/

double			compute_elapsed_time(struct timeval start, struct timeval end)
{
	return (((double)((double)end.tv_sec - (double)start.tv_sec) * 1000) +
		(double)((double)end.tv_usec - (double)start.tv_usec) / 1000);
}

/*
** calcule la latence moyenne en ajoutant le round trip a la fin, save le temps passe
**  s'il est < ou > aux actuelles minimum/maximum valeurs
*/

void			save_elapsed_time(double time_elapsed)
{
	g_env.sum_time += time_elapsed;
	g_env.square_sum_time += time_elapsed * time_elapsed;
	if (g_env.min_time > time_elapsed || g_env.min_time == 0)
		g_env.min_time = time_elapsed;
	if (g_env.max_time < time_elapsed || g_env.max_time == 0)
		g_env.max_time = time_elapsed;
}

/*
** fsave le temps dans une struc timeval.
*/

void			save_current_time(struct timeval *destination)
{
	if (gettimeofday(destination, NULL) == -1)
		error_output_and_exit(TIMEOFDAY_ERROR);
}

/*
** round trip a besoin de la deviation de latence standard (mdev), i.e. la distance moyenne entre une
**  latence de ping et la latence moyenne. return ca en se basant sur
**  [mdev = SQRT(average square time - average time ^2)]
*/

double			time_standard_deviation(void)
{
	double		average_time;
	double		average_squared_time;

	average_time = g_env.sum_time / g_env.received_packets;
	average_squared_time = g_env.square_sum_time / g_env.received_packets;
	return (sqrt(average_squared_time - (average_time * average_time)));
}